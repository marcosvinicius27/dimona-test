<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/imask/6.1.0/imask.min.js"></script>
    <title>Formulário Cliente</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="/" style="color:white">DIMONA</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto ml-md-0 justify-content-end">
                    @if (Route::has('login'))
                    @auth
                    <li class="nav-item">
                        <a href="{{ url('/dashboard') }}" class="nav-link font-weight-bold">Dashboard</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a href="{{ route('login') }}" class="nav-link font-weight-bold">Log in</a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a href="{{ route('register') }}" class="nav-link font-weight-bold">Register</a>
                    </li>
                    @endif
                    @endauth
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container body-content mt-5">
        <div class="row justify-content-md-center">
            <div class="col-md-9 col-xs-12 mr-10">
                @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                <form method="POST" action="clientes" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="nome_fantasia" class="form-label">Nome Fantasia:</label>
                                <input type="text" id="nome_fantasia" name="nome_fantasia" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="cnpj" class="form-label">CNPJ:</label>
                                <input type="text" id="cnpj" name="cnpj" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="endereco" class="form-label">Endereço:</label>
                                <input type="text" id="endereco" name="endereco" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="cidade" class="form-label">Cidade:</label>
                                <input type="text" id="cidade" name="cidade" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="estado" class="form-label">Estado:</label>
                                <input type="text" id="estado" name="estado" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="pais" class="form-label">País:</label>
                                <input type="text" id="pais" name="pais" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="telefone" class="form-label">Telefone:</label>
                                <input type="text" id="telefone" name="telefone" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="email" class="form-label">E-mail:</label>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="area_atuacao" class="form-label">Área de Atuação | CNAE:</label>
                        <input type="text" id="area_atuacao" name="area_atuacao" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label for="quadro_societario" class="form-label">Quadro Societário:</label>
                        <textarea id="quadro_societario" name="quadro_societario" class="form-control"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#cnpj").on("input", function() {
                // Remova caracteres não numéricos
                var cnpj = $(this).val().replace(/\D/g, '');

                if (cnpj.length === 14) {
                    // Realize a solicitação AJAX à API externa

                    var param = {};
                    param.url = 'https://www.receitaws.com.br/v1/cnpj/' + cnpj;
                    param.method = 'GET';
                    param.success = function(data) {
                        console.log(data);
                    };
                    param.dataType = 'jsonp';
                    serviceRest(param);
                }
            });
        });

        function serviceRest(param) {

            $.ajax({
                url: param.url,
                dataType: param.dataType,
                type: param.method,
                data: param.data,
                success: param.success
            });

        }
    </script>

    <script>
        // Máscaras
        var cnpjMask = IMask(document.getElementById('cnpj'), {
            mask: '00.000.000/0000-00'
        });

        var telefoneMask = IMask(document.getElementById('telefone'), {
            mask: '(00) 00000-0000'
        });
    </script>


</body>

</html>