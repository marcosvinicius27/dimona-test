<?php

namespace App\Repositories;

use App\Models\Cliente;

class ClienteRepository
{
    
    public function postCliente($dados)
    {
        // Crie um novo cliente com base nos dados fornecidos
        $cliente = new Cliente([
            'nome_fantasia' => $dados['nome_fantasia'],
            'cnpj' => $dados['cnpj'],
            'endereco' => $dados['endereco'],
            'cidade' => $dados['cidade'],
            'estado' => $dados['estado'],
            'pais' => $dados['pais'],
            'telefone' => $dados['telefone'],
            'email' => $dados['email'],
            'area_atuacao_cnae' => $dados['area_atuacao'],
            'quadro_societario' => $dados['quadro_societario'],
        ]);

        // Salve o novo cliente no banco de dados
        $cliente->save();

        // Retorne o cliente criado
        return $cliente;
    }

}