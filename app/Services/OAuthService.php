<?php

namespace App\Services;

use GuzzleHttp\Client;

class OAuthService
{
    public function requestNewToken()
    {
        $client = new Client([
            'verify' => false,
        ]);

        $response = $client->request('POST', 'http://192.168.128.1:8080/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => env('OAUTH_CLIENT_ID'),
                'client_secret' => env('OAUTH_CLIENT_SECRET'),
                'username' => env('OAUTH_USERNAME'),
                'password' => env('OAUTH_PASSWORD'),
            ],            
        ]);

        // Processar a resposta
        $responseData = json_decode($response->getBody(), true);

        return $responseData;
    }
}
