<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Repositories\ClienteRepository;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Response as HttpResponse;

class ClienteService
{
    protected $ClienteRepository;

    public function __construct(ClienteRepository $ClienteRepository)
    {
        $this->ClienteRepository = $ClienteRepository;
    }

    public function PostCliente($dados)
    {
        
        $returnPostCliente = $this->ClienteRepository->PostCliente($dados);

        return[
                'returnPostCliente' => $returnPostCliente,
                'status' => HttpResponse::HTTP_OK
        ];
       
    }
}
