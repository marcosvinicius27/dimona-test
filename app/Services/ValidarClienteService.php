<?php 

namespace App\Services;
use Illuminate\Support\Facades\Validator;

class ValidarClienteService
{

    public function validarCliente($dados)
    {
       // Regras de validação
       $regras = [
        'nome_fantasia' => 'required|string|max:255',
        'cnpj' => 'required', // Certifique-se de ajustar a regex para o formato do CNPJ
        'endereco' => 'required|string|max:255',
        'cidade' => 'required|string|max:255',
        'estado' => 'required|string|max:255',
        'pais' => 'required|string|max:255',
        'telefone' => 'required|string|max:20',
        'email' => 'required|email',
        'area_atuacao' => 'required|string|max:255',
        'quadro_societario' => 'nullable|string', // O campo é opcional
        ];

    // Mensagens de erro personalizadas
    $mensagens = [
        'nome_fantasia.required' => 'O campo Nome Fantasia é obrigatório.',
        'nome_fantasia.string' => 'O campo Nome Fantasia deve ser uma string.',
        'nome_fantasia.max' => 'O campo Nome Fantasia não pode exceder 255 caracteres.',
        'cnpj.required' => 'O campo CNPJ é obrigatório.',
        'endereco.required' => 'O campo Endereço é obrigatório.',
        'endereco.string' => 'O campo Endereço deve ser uma string.',
        'endereco.max' => 'O campo Endereço não pode exceder 255 caracteres.',
        'cidade.required' => 'O campo Cidade é obrigatório.',
        'cidade.string' => 'O campo Cidade deve ser uma string.',
        'cidade.max' => 'O campo Cidade não pode exceder 255 caracteres.',
        'estado.required' => 'O campo Estado é obrigatório.',
        'estado.string' => 'O campo Estado deve ser uma string.',
        'estado.max' => 'O campo Estado não pode exceder 255 caracteres.',
        'pais.required' => 'O campo País é obrigatório.',
        'pais.string' => 'O campo País deve ser uma string.',
        'pais.max' => 'O campo País não pode exceder 255 caracteres.',
        'telefone.required' => 'O campo Telefone é obrigatório.',
        'telefone.string' => 'O campo Telefone deve ser uma string.',
        'telefone.max' => 'O campo Telefone não pode exceder 20 caracteres.',
        'email.required' => 'O campo E-mail é obrigatório.',
        'email.email' => 'O campo E-mail deve ser um endereço de e-mail válido.',
        'area_atuacao.required' => 'O campo Área de Atuação | CNAE é obrigatório.',
        'area_atuacao.string' => 'O campo Área de Atuação | CNAE deve ser uma string.',
        'area_atuacao.max' => 'O campo Área de Atuação | CNAE não pode exceder 255 caracteres.',
    ];
    // Realize a validação dos dados
    $validador = Validator::make($dados, $regras, $mensagens);

    
    
    return $validador;
    
    }
}
