<?php

namespace App\Services;

use GuzzleHttp\Client;

class GuzzleService
{
    public function guzzleConecction($method,$url,$data,$token)
    {
        $client = new Client([
            'verify' => false,
        ]);
    
        $headers = [
            'Authorization' => 'Bearer '.$token,
        ];

        
        $response = $client->request($method, $url, [
            'form_params' => $data,
            'headers' => $headers,
        ]);
    
        // Processar a resposta
        $responseData = json_decode($response->getBody(), true);
    
        return $responseData;
    }
}
