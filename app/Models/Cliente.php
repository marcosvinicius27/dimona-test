<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nome_fantasia',
        'cnpj',
        'endereco',
        'cidade',
        'estado',
        'pais',
        'telefone',
        'email',
        'area_atuacao_cnae',
        'quadro_societario',
    ];


        
}
