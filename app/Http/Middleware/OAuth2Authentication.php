<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use Laravel\Passport\Bridge\AccessToken;
use App\Services\OAuthService;

class OAuth2Authentication
{
    protected $oauthService;

    public function __construct(OAuthService $oauthService)
    {
        $this->oauthService = $oauthService;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        // Verifique se o token já está na sessão
        if (Session::has('oauth2_token')) {
            $token = Session::get('oauth2_token');

            // Verifique se o token está expirado
            $expirationTimestamp = time() + $token['expires_in'];

            if ($expirationTimestamp <= time()) {

                $newToken = $this->oauthService->requestNewToken();

                // Salvar o novo token na sessão para uso posterior
                Session::put('oauth2_token', $newToken);
            }
        }

        return $next($request);
    }
}
