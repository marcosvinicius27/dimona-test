<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ValidarClienteService;
use App\Services\GuzzleService;
use Illuminate\Support\Facades\Session;

class ClienteController extends Controller
{

    protected $validarClienteService;
    protected $GuzzleService;

    public function __construct(ValidarClienteService $validarClienteService, GuzzleService $guzzleService)
    {
        $this->validarClienteService = $validarClienteService;
        $this->GuzzleService = $guzzleService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // Obtenha os dados da requisição
        $dados = $request->all();

        // Chame a função do serviço
        $cliente = $this->validarClienteService->validarCliente($dados);

        if ($cliente->fails()) {
            return view('welcome')->with('errors', $cliente->errors());
        }

        $token = Session::get('oauth2_token.access_token');

        $apiUrl = env('URL_API') . '/api/ApiClientes';

        $cliente = $this->GuzzleService->guzzleConecction('POST',$apiUrl,$dados,$token);


        return view('welcome')->with('success', 'Cliente Inserido com Sucesso');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
